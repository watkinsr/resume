.\"
.\" ----------------------------------------------------------------
.po 3c
.sp 3c
.nr Ll 15c \" line length
.nr Dw 3c  \" date width
.nr Cw .5c \" column separator width
.nr Tw \n[Ll]u-\n[Dw]u-\n[Cw]u \" description text width
.\" ----------------------------------------------------------------
.de Date
.in 0
.ft 2
.ll \\n[Dw]u
.ad l
.di Dd
..
.de Section
.in 0
.ft 3
.ll \\n[Ll]u
.ad l
..
.de Title
.in 0
.fzoom 10 1500
.ft 10
.ll \\n[Ll]u
.ad c
.Dd
..
.de Company
.br
.di
.nr Dh \\n[dn]
.in 0
.ft 3
.ll \\n[Tw]u
.ad l
.di Td
..
.de Source
.br
.ne \\n[Dh]u+5v
.ll \\n[Ll]u
.in 0
.mk
.nf
.rt
.in \\n[Dw]u+\\n[Cw]u
.ft 7
.ad b
..
.de Desc
.br
.di
.if \\n[dn]>\\n[Dh] .nr Dh \\n[dn]
.ne \\n[Dh]u+5v
.sp 2
.ll \\n[Ll]u
.in 0
.mk
.nf
.Dd
.rt
.in \\n[Dw]u+\\n[Cw]u
.Td
.rt
.sp \\n[Dh]u
.in 0
\v'-.5m'\D'l \\n[Ll]u 0'
.in \\n[Dw]u+\\n[Cw]u
.fi
.ft 1
.ad b
..
.de BL
.in +2n
..
.de LE
.sp .2
.in -2n
..
.de LI
.sp .2
\Z'\h'-1.3n'\[bu]'\c
..
.\" ----------------------------------------------------------------
.Title
Ryan Watkins
.\" ----------------------------------------------------------------


.Section
Education
.\" ----------------------------------------------------------------
.Date 
2016
.Company
BSc. Computer Science (First Class Hons.), University of Reading
.Desc
.\" ----------------------------------------------------------------

.Section
Location:
.\" ----------------------------------------------------------------
.ft 5
London(SM6) or Remote
.\" ----------------------------------------------------------------


.Section
Skills
.\" ----------------------------------------------------------------
  
.I NodeJS,
.I Clojure,
.I Shell-scripting,
.I C,
.I Scala,
.I Golang,
.I AWS,
.I UNIX,
.I MongoDB
.\" ----------------------------------------------------------------

.Section
Work Experience
.\" ----------------------------------------------------------------
.Date
09/17 - 09/18
.Company
Red Ant (Full Stack Developer)
.Desc
The role entailed;
.BL
.LI
Data migration for MHRA from SQL to MongoDB
.LI
Delivering mobile applications (React-Native) for clients such as Mastercard / MHRA currently in app stores.
.LI
Delivered backend for aforementioned applications in NodeJS including implementing legacy features from a PHP codebase.
.LI
Worked on flagship project for lots of retailers.
.Date
08/16 - 02/17
.Company
Sixt (Junior Engineer)
.Desc
The role entailed:
.BL
.LI
Using React and Redux on the frontend to integrate with a Golang Middleware that interacted with microservices.
.LI
Helped on the Golang middleware
.LE
.Date
07/15 - 10/15
.Company
Risklab GmbH (Frontend Internship)
.Desc
The role entailed:
.BL
.LI
Design UI for "excel-like" usage in the browser for a given .NET app that showed futures / financial instruments.
.LE
.bp
.\" ----------------------------------------------------------------
.Section
Projects
.\" ----------------------------------------------------------------
.Date
2020
.Company
Herrostats
.Desc
An app to display Tyler Herro's latest stats from the NBA stats API (Clojure)
.Source
https://gitlab.com/watkinsr/herrostats
.Date
2016
.Company
SQASM
.Desc
A quantum programming language
.Source
https://github.com/watkinsr/SQASM

.\" ----------------------------------------------------------------
.Section
Certifications
.\" ----------------------------------------------------------------
.Date
2020
.Company
.ft 5
Functional Program Design in Scala
Functional Programming Principles in Scala
Parallel Programming
Big Data Analysis with Scala and Spark
.Desc

.\" ----------------------------------------------------------------
.Section
Community Involvement
.\" ----------------------------------------------------------------
.Date
2020
.Company
Funtoo Linux
.Desc
Updating various ebuilds for the distribution

.Section
DevOps
.\" ----------------------------------------------------------------
  
.I Jenkins,
.I Gitlab 
.I Pipelines,
.I Bitbucket 
.I Pipelines,
.I Bitrise
.\" ----------------------------------------------------------------


.\" ----------------------------------------------------------------
.Section
Human Languages:
.\" ----------------------------------------------------------------
.ft 5
English (native),
German (A1),
Chinese (HSK1)











References given on request
